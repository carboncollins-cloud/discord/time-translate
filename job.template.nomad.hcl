job "discord-time-translator-bot" {
  name = "Discord Time Translator (Bot)"
  type = "service"
  region = "se"
  datacenters = ["soc"]
  namespace = "c3n-discord"

  group "discord-bot" {
    count = 1

    service {
      name = "discord-time-translator-bot"
      task = "time-translator"
    }

    task "time-translator" {
      driver = "docker"

      config {
        image = "[[ .jobDockerImage ]]"
      }

      env {
        DISCORD_TOKEN = ""
      }
    }
  }

  reschedule {
    delay = "10s"
    delay_function = "exponential"
    max_delay = "10m"
    unlimited = true
  }

  update {
    max_parallel = 1
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "10m"
    progress_deadline = "15m"
    auto_revert       = true
    auto_promote      = true
    stagger = "30s"
    canary = 1
  }

  meta {
    gitSha = "[[ .gitSha ]]"
    gitBranch = "[[ .gitBranch ]]"
    pipelineId = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId = "[[ .projectId ]]"
    projectUrl = "[[ .projectUrl ]]"
  }
}
