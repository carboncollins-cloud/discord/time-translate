import { ChatInputCommandInteraction, SlashCommandBuilder } from 'discord.js';
import type { SlashCommand } from '../../types.d.ts';

const command = new SlashCommandBuilder()
  .setName('time')
  .setDescription('Prints the current time localised for each user')
  .addSubcommand(subCommand => subCommand
    .setName('now')
    .setDescription('Prints the current time'))
  .addSubcommand(subCommand => subCommand
    .setName('add')
    .setDescription('Prints the time with a specified offset')
    .addIntegerOption(option => option
      .setName('amount')
      .setDescription('Add an amount of offset to the timer')
      .setRequired(true))
    .addIntegerOption(option => option
      .setName('interval')
      .setDescription('Select an interval for the amount')
      .setRequired(true)
      .addChoices(
        { name: 'Days', value: 86_400 },
        { name: 'Hours', value: 3_600 },
        { name: 'Minutes', value: 60 }
      ))
    .addStringOption(option => option
      .setName('display')
      .setDescription('Determins howthe time will be displayed (default: DateTime)')
      .setRequired(false)
      .addChoices(
        { name: 'Date', value: 'D' },
        { name: 'Time', value: 'T' },
        { name: 'DateTime', value: 'F' },
        { name: 'Relative', value: 'R'}
      ))
  )

async function execute(interaction: ChatInputCommandInteraction) {
  try {
    await interaction.deferReply({ ephemeral: true });
    const now = Math.trunc(Date.now() / 1_000);

    const subCommand = interaction.options.getSubcommand(true);

    if (subCommand === 'now') {
      await interaction.channel?.send(`Time is currently: <t:${now}>`);
    } else if (subCommand === 'add') {
      const amount = interaction.options.getInteger('amount', true);
      const interval = interaction.options.getInteger('interval', true);
      const display = interaction.options.getString('display', false) ?? 'F'

      const intervalText = interval === 86_400 ? 'day' : (interval === 3_600 ? 'hour' : 'minute');
      const intervalPlural = amount === 1 ? '' : 's'

      const futureTime = now + (amount * interval);

      await interaction.channel?.send(`Time in ${amount} ${intervalText}${intervalPlural} time: <t:${futureTime}:${display}>`);
    }

    await interaction.deleteReply();
  } catch (err: unknown) {
    await interaction.editReply("Something went wrong...");
  }
}

const nowCommand: SlashCommand = { command, execute };

export default nowCommand;
