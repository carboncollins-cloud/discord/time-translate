import { Client, REST, Routes, SlashCommandBuilder } from 'discord.js';

import now from './now.js';

export function registerSlashCommands(client: Client): void {
  const slashCommands : SlashCommandBuilder[] = [
    now.command
  ]

  client.slashCommands.set(now.command.name, now);

  client.on('ready', () => {
    if (client?.application === null) throw new Error('Client Application missing');

    new REST({ version: '10' })
    .setToken(process.env.DISCORD_TOKEN)
    .put(Routes.applicationCommands(client.application.id), {
      body: slashCommands.map(command => command.toJSON())
    })
    .then((data: unknown) => {
      console.log('Registered / commands');
    })
    .catch((err: unknown) => {
      console.error('Failed to register / commands', err);
    });
  });
}
