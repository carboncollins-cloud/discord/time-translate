import { Client, GatewayIntentBits, Collection } from 'discord.js';

import type { Command, SlashCommand } from './types.d.ts';

import { registerSlashCommands } from './handlers/slashCommands/index.js';

import { registerInteractionCreateEvent } from './events/interactionCreate.js';

if (!process.env.DISCORD_TOKEN) throw new Error('Missing "DISCORD_TOKEN" environment variable');

const client = new Client({
  intents: [
    GatewayIntentBits.Guilds,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.DirectMessages,
    GatewayIntentBits.MessageContent
  ]
});

client.slashCommands = new Collection<string, SlashCommand>();
client.commands = new Collection<string, Command>()
client.cooldowns = new Collection<string, number>()

registerSlashCommands(client);

registerInteractionCreateEvent(client);

await client.login(process.env.DISCORD_TOKEN);
