FROM node:18 AS build-env
COPY . /app
WORKDIR /app

RUN yarn install --frozen-lockfile --non-interactive
RUN yarn build

FROM node:18 AS prod-deps
COPY . /app
WORKDIR /app

RUN yarn install --frozen-lockfile --non-interactive --prod

FROM gcr.io/distroless/nodejs18-debian11

COPY --from=build-env /app/package.json /app/package.json
COPY --from=build-env /app/build /app/build
COPY --from=prod-deps /app/node_modules /app/node_modules

WORKDIR /app

CMD ["build/index.js"]
